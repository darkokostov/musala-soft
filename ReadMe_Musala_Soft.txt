# My Cypress Test Project

This project contains automated tests using Cypress for the Musala Soft website.

## Installation

1. Clone this repository.
2. Navigate to the project directory.
3. Run `npm install` to install dependencies.

## Usage

To run tests in Chrome:
npm run test:chrome


To run tests in Firefox:
npm run test:firefox


Parallel Execution:
npm run test:parallel


IMPORTANT NOTE: "Test Case 2: Company and Facebook Links" is not fully complete. The fully needed code is there, but Cypress commands
would not execute when redirecting to Musala Soft's Faceboook page in a new tab. I have spent 4-5 hours trying 10+ solutions to make it work, but yet, it could not work. The last line of code within the test case is the one that is not executed. 