describe('Musala Soft Website Testing', () => {
  
  beforeEach(() => {
    cy.viewport(1920, 1080); 
  });

  it('Test Case 1: Invalid Email Format', () => {
    // Step 1: Visit http://www.musala.com/
    cy.visit('http://www.musala.com/');

    // Step 2: Scroll down and go to 'Contact Us'
    cy.get('button.contact-label.btn.btn-1b').scrollIntoView().click({force: true});

    // Step 3: Fill all required fields except email
    cy.get('#cf-1').type('Darko Kostov');
    cy.get('#cf-3').type('1234567890');
    cy.get('#cf-4').type('Test');
    cy.get('#cf-5').type('Test Test');

    // Step 4, 5, and 6 for each invalid email
    cy.readFile('cypress/fixtures/InvalidEmails.txt').then(invalidEmails => {
      const emailsArray = invalidEmails.split('\n');
      emailsArray.forEach(email => {
      cy.get('#cf-2').clear().type(email);
      cy.contains('Send').click();
      cy.contains('The e-mail address entered is invalid.').should('exist');
    });
  });
});


  it('Test Case 2: Company and Facebook Links', () => {
    // Step 1: Visit http://www.musala.com/
    cy.visit('http://www.musala.com/');

    // Step 2: Click 'Company' tab from the top
    cy.contains('Company').click({force: true});

    // Step 3: Verify correct URL
    cy.url().should('eq', 'https://www.musala.com/company/');

    // Step 4: Verify 'Leadership' section
    cy.contains('Leadership').should('exist');
    cy.wait(3000);

    // Step 5: Click the Facebook link from the footer
    cy.get('span.musala.musala-icon-facebook').click({ force: true });
    cy.wait(3000);
    cy.visit('https://facebook.com/MusalaSoft?fref=ts');
    cy.url().should('eq', 'https://www.facebook.com/MusalaSoft?fref=ts');
    cy.get('circle.xbh8q5q.x1pwv2dq.xvlca1e').should('be.visible');
  }); 


  it('Test Case 3: Careers and Job Application', () => {
    // Step 1: Visit http://www.musala.com/
    cy.visit('http://www.musala.com/');

    // Step 2: Navigate to Careers menu (from the top)
    cy.contains('Careers').click({force: true});

    // Step 3: Click 'Check our open positions' button
    cy.contains('Check our open positions').click();

    // Step 4: Verify 'Join Us' page URL
    cy.url().should('eq', 'https://www.musala.com/careers/join-us/');

    // Step 5: Select 'Anywhere' from the dropdown 'Select location'
    cy.get('#get_location').select('Anywhere');

    // Step 6: Choose open position by name
    cy.contains('DevOps Engineer').click();

    // Step 7: Verify 4 main sections
    cy.contains('General description').should('exist');
    cy.contains('Requirements').should('exist');
    cy.contains('Responsibilities').should('exist');
    cy.contains('What we offer').should('exist');

    // Step 8: Verify 'Apply' button
    cy.get('input.wpcf7-submit.btn-join-us.btn-apply').should('exist');

    // Step 9: Click 'Apply' button
    cy.get('input.wpcf7-submit.btn-join-us.btn-apply').click({force: true});

    // Step 10: Prepare a few sets of negative data and verify error messages
    // Repeat the following steps for each negative data set
    cy.get("#cf-2").type('musalatest@test');
    
    cy.fixture('CV.pdf').then(fileContent => {
      cy.get('input[type="file"]').attachFile({
        fileContent,
        fileName: 'CV.pdf',
        mimeType: 'application/pdf'
      });
    });
    
    cy.contains('Send').click({force: true});

    cy.get('span.wpcf7-not-valid-tip').contains('The field is required.').should('exist');
    cy.get('span.wpcf7-not-valid-tip').contains('The e-mail address entered is invalid.').should('exist');
  });


  it('Test Case 4: Filter and Print Open Positions by City', () => {
    // Step 1: Visit http://www.musala.com/
    cy.visit('http://www.musala.com/');

    // Step 2: Navigate to Careers menu (from the top)
    cy.contains('Careers').click({force: true});

    // Step 3: Click 'Check our open positions' button
    cy.contains('Check our open positions').click();

    // Step 4: Filter available positions by available cities
    cy.get('#get_location').select('Sofia');

    // Step 5: Get the open positions by city and print
    cy.get('h2.card-jobsHot__title').each((position) => {
      const positionTitle = position.text();
      const positionLink = position.closest('article.card-jobsHot').find('a.card-jobsHot__link').attr('href');
      cy.log('Sofia');
      cy.log(`Position: ${positionTitle}`);
      cy.log(`More info: ${positionLink}`);
    });

    // Repeat the same steps for Skopje
    cy.get('#get_location').select('Skopje');
    cy.get('h2.card-jobsHot__title').each((position) => {
      const positionTitle = position.text();
      const positionLink = position.closest('article.card-jobsHot').find('a.card-jobsHot__link').attr('href');
      cy.log('Skopje');
      cy.log(`Position: ${positionTitle}`);
      cy.log(`More info: ${positionLink}`);
    });
  });
});
